﻿using System;

public class Nevera
{
	public int fridge;
	public int amount;

	public Nevera()
	{
		fridge = 6;
	}

	public void FillFridge(string name)
	{
		Console.WriteLine("Hi han " + fridge + "cerveces");
		Random random = new Random();
		if(fridge <= 9)
		{
			fridge += random.Next(0, 7);
			amount = fridge;
		}

		if(fridge > 9) fridge = 9;
		Console.WriteLine(name + " omple la nevera amb " + amount + " cerveses."); 

	}

	public void DrinkBeers(string name)
	{
        Console.WriteLine("Hi han " + fridge + "cerveces");
        Random random = new Random();
        if (fridge > 0)
        {
            fridge += random.Next(0, 7);
            amount = fridge;
        }

        if (fridge < 0) fridge = 0;
        Console.WriteLine(name + " beu " + amount + " de cerveses.");
    }
}
