﻿using System;

public class Program
{

	/*//EXERCICI 1 

	public static void Main(string[] args)
	{
		string frase1 = " Una vegada hi havia un gat";
		string frase2 = "En un lugar de la Mancha";
		string frase3 = "Once upon a time in the west";

		Thread thread1 = new Thread(() =>
		{
			Console.WriteLine(frase1);
			thread1.sleep(1000);
        });

		Thread thread2 = new Thread(() =>
		{
			thread1.Join();
            Console.WriteLine(frase2);
			thread2.sleep(1000);
		});

        Thread thread3 = new Thread(() =>
        {
            thread2.Join();
            Console.WriteLine(frase3);
            thread3.sleep(1000);
        });

        thread1.Start();
		thread2.Start();
		thread3.Start();
		
		
	}*/

	//EXERCICI 2

	public static void Main()
	{
		var numBeer = new Fridge();

		Thread thread1 = new Thread(() => numBeer.FillFridge("Anitta"));
        Thread thread2 = new Thread(() =>
        {
            thread1.Join();
			numBeer.DrinkBeers("Bad Bunny");
            
        });

        Thread thread3 = new Thread(() =>
        {
            thread2.Join();
            numBeer.DrinkBeers("Lil Nas X");

        });

        Thread thread4 = new Thread(() =>
        {
            thread3.Join();
            numBeer.DrinkBeers("Manuel Turizo");

        });

		thread1.Start();
		thread2.Start();
		thread3.Start();
		thread4.Start();
    }
}
